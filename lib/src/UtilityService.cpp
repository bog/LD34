#include <UtilityService.hpp>

UtilityService::UtilityService() : Service()
{
  
}

std::vector<std::string> UtilityService::split(std::string str, char separator, char endSeparator) const
{
  std::vector<std::string> res;
  std::string word;
  
  for ( char letter : str )
    {
      if ( letter != separator && letter != endSeparator)
	{
	  word += letter;
	}
      else
	{
	  res.push_back(word);
	  word = "";
	}
    }
  return res;
}

std::string UtilityService::nucleus(std::string path)
{
  // the size of the file as : path/nucleus.extension 
  size_t nucleusLength = path.rfind('.') - path.rfind('/') - 1;

  // in above exemple, nucleus is "nucleus" 
  std::string nucleus = path.substr( path.rfind('/') + 1, nucleusLength );

  return nucleus;
}

std::string UtilityService::nucleusAndExtension(std::string path)
{
    // the size of the file as : path/nucleus.extension 
  size_t endLength = path.rfind('/') - 1;

  // in above exemple, nucleus is "nucleus" 
  std::string end = path.substr( endLength, -1 );

  return end;
}


UtilityService::~UtilityService()
{
  
}
