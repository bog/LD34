#include <ResourceSystem.hpp>
#include <ServiceLocator.hpp>

ResourceSystem::ResourceSystem()
{
  //  sl->event()->trigger("log", std::make_shared<LogEvent>("ResourceEngine","initializing..."));  
  sl->utility()->load("../scripts/textures.config",this, &ResourceSystem::loadTexture);
  sl->utility()->load("../scripts/sounds.config",this, &ResourceSystem::loadSound);
  sl->utility()->load("../scripts/musics.config",this, &ResourceSystem::loadMusic); 
}
sf::Texture&  ResourceSystem::getTexture(std::string id) 
{
  if ( m_textures.find(id) == m_textures.end() )
    {
      std::cerr<< "[ResourceSystem] Can't find the texture of id : " + id <<std::endl;
    }
  return m_textures[id];
}

sf::FloatRect& ResourceSystem::getTextureCoords(std::string id)
{
  if ( m_texCoords.find(id) == m_texCoords.end() )
    {
      std::cerr<< "[ResourceSystem] Can't find coords of id : " + id <<std::endl;
    }
  return m_texCoords[id];
}

void ResourceSystem::loadTexture(std::string line)
{
  auto words = sl->utility()->split(line, ' ', ';');
  switch( line[0] )
    {
    default : /* error event */ break;
    case 'T' :
      {
	std::string id = sl->utility()->nucleus(words[1]);
	sf::Texture texture;
	if ( !texture.loadFromFile(words[1]) )
	  {
	    throw std::invalid_argument("[ResourceEngine] can't read line : " + line + ".");
	  }
	m_textures.insert(std::make_pair(id,texture));
      }break;

          case 'C' :
      {
	std::string id = words[1];
	sf::FloatRect rect;
	if ( words.size() < 6 ) // C id x y w h
	  {
	    throw std::invalid_argument("[ResourceEngine] can't read line : " + line + ".");
	  }
	  float x = std::atof(words[2].c_str());
	  float y = std::atof(words[3].c_str());
	  float w = std::atof(words[4].c_str());
	  float h = std::atof(words[5].c_str());
				  
	  m_texCoords.insert(std::make_pair(id,sf::FloatRect(x,y,w,h)));
	  
      }break;
    }
}

void ResourceSystem::loadSound(std::string line)
{
    auto words = sl->utility()->split(line, ' ', ';');
  // security
  if ( words.size() != 2 ) // sound /path/to/sound
    {
      throw std::invalid_argument("[ResourceSystem] loadSound read bad sentance from config file.");
    }
 
  sf::SoundBuffer buffer;
  if ( !buffer.loadFromFile(words[1]) )
    {
      throw std::invalid_argument("[ResourceSystem] loadSound tried to load an unexisting sound from config file : " + words[1] + ".");
    }
  
  m_sounds.insert( std::pair<std::string,sf::SoundBuffer>(words[0], buffer ) );
  // I have issues with the make_pair; don't really know how to use and have to make it quick
}

sf::SoundBuffer& ResourceSystem::getSoundBuffer(std::string id)
{
  if ( m_sounds.find(id) == m_sounds.end() )
    {
      std::cerr<< "[ResourceSystem] Can't find sound of id : " + id <<std::endl;
    }
  return m_sounds[id];
}

void ResourceSystem::loadMusic(std::string line)
{
  m_musicPathes.insert( std::pair<std::string, std::string>(sl->utility()->nucleus(line), line));
}

std::string ResourceSystem::getMusicPath(std::string id)
{
  if ( m_musicPathes.find(id) == m_musicPathes.end() )
    {
      throw std::invalid_argument("[ResourceSystem] getMusicPath : can't find path for id : " + id + ".");
    }

  return m_musicPathes[id];
}

ResourceSystem::~ResourceSystem()
{
  
}
