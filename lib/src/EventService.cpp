#include <EventService.hpp>

EventService::EventService()
{
  
}

void EventService::subscribe(const std::string id, const EventHandler handler)
{
  auto itr = m_subscribers.find(id); // find the id

  if(itr == m_subscribers.end()) // first occurence of this id
    {
      // We create the place then store the function
      m_subscribers.insert(std::make_pair(id, std::vector<EventHandler>()));
    }

  // We had the function to the others
  m_subscribers[id].push_back(handler);
}

void EventService::trigger(const std::string id)
{
  Event event;
  trigger(id, event);
}

void EventService::trigger(const std::string id, Event& event)
{
  auto itr = m_subscribers.find(id);

  if( itr == m_subscribers.end() )
    {
      std::cerr << " [EventSystem] Couldn't activate unregistered event : " << id << " ." << std::endl;
      return;
    }

  std::vector<EventHandler> &vec = itr->second;
  std::vector<EventHandler> keep;
    
  for(EventHandler& eh : vec)
    {
      if( eh(event) == EventStatus::Continue )
	{
	  keep.push_back(eh);
	}
    }

  vec.swap(keep);

}

EventService::~EventService()
{
  
}
