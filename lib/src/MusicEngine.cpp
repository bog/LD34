#include <MusicEngine.hpp>
#include <ResourceSystem.hpp>
#include <stdexcept>

MusicEngine::MusicEngine( std::shared_ptr<ResourceSystem> resources ) :
  m_resources(resources)
{
  sl->event()->subscribe("music:play", this, &MusicEngine::play);
  m_music.setLoop(true);
}

EventStatus MusicEngine::play(Event& event)
{
  MusicEvent& me = static_cast<MusicEvent&>(event);

  std::string path = m_resources->getMusicPath( me.id );
  
  if ( !m_music.openFromFile(path) )
    {
      throw std::invalid_argument("[MusicEngine] Play: cannot play the file : " + me.id + ".");
    }
  
  m_music.play();
  
  return EventStatus::Continue;
}

MusicEngine::~MusicEngine()
{
  
}
