#include <SoundEngine.hpp>
#include <ResourceSystem.hpp>

SoundEngine::SoundEngine(std::shared_ptr<ResourceSystem> resources) :
  m_resources(resources)
{
  sl->event()->subscribe("sound:play", this, &SoundEngine::play);
}

EventStatus SoundEngine::play(Event& e)
{
  SoundEvent& se = static_cast<SoundEvent&>(e);
  
  sf::SoundBuffer& buffer = m_resources->getSoundBuffer(se.id);
  m_sounds.emplace_back(buffer);
  m_sounds.back().play(); // activates the sound
  // We can localize the sound with Sound::setPosition(x,y,z), could be interesting here

  if ( m_sounds.size() >= MAX_STORED_SOUND_NUMBER )
    {
      clearStopped();
    }
  
  return EventStatus::Continue;
}

void SoundEngine::clearStopped(void)
{
  std::vector<sf::Sound> vec;
  for( sf::Sound sound : m_sounds )
    {
      if ( sound.getStatus() == sf::SoundSource::Status::Playing )
  	{
  	  vec.push_back( std::move(sound) );
  	}
    }
  m_sounds.swap(vec);
}

SoundEngine::~SoundEngine()
{
  
}
