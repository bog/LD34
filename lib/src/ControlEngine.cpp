#include <ControlEngine.hpp>
#include <GameEvents.hpp>

ControlEngine::ControlEngine()
  : m_key_count(0)
  , m_key_delay(500)
  , m_key_previous(Key::None)
{
  sl->event()->subscribe("key:pressed", [this](Event& event){
      KeyEvent& ke = static_cast<KeyEvent&>(event);
      KeyEvent* key_tmp = new KeyEvent;
      key_tmp->key = ke.key;

      m_key_count++;
      m_key_previous = ke.key;
      // std::cout<< "pressed" <<std::endl; // DEBUG
      
      sl->time()->waitAnd(m_key_delay, [this](void* data){
      	  KeyEvent* k = (KeyEvent*)(data);
	  
      	  //std::cout<< m_key_count <<std::endl; // DEBUG
	  
	  if(m_key_previous != k->key)
	    {
	      k->key = Key::Both;
	    }
	  
	  sl->event()->trigger("key", *k);

	  m_key_count = 0;
	  m_key_previous = k->key;
	  delete k;
      	}, key_tmp);
      

      return EventStatus::Continue;
    });
}

ControlEngine::~ControlEngine()
{
  
}
