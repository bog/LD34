#include <RenderEngine.hpp>
#include <stdexcept>
#include <ResourceSystem.hpp>

RenderEngine::RenderEngine(std::shared_ptr<sf::RenderWindow> window, std::shared_ptr<ResourceSystem> resources) :
  m_window(window)
  , m_resources(resources)
{
  // Security
  if( !window ){ throw std::invalid_argument("E : Window can't be nullptr !"); }
  if( !resources ){ throw std::invalid_argument("E : ResourceEngine can't be nullptr !"); }
  
  m_rectangle.setFillColor(sf::Color::Black);

  // Subscribe
  sl->event()->subscribe("render:add", this, &RenderEngine::add);
  sl->event()->subscribe("render:addText", this, &RenderEngine::addText);
  sl->event()->subscribe("render:clearText", this, &RenderEngine::clearText);
  sl->event()->subscribe("render:window", this, &RenderEngine::getWindow);

  // TEXT & FONT
  std::string font_path = RSC_PATH;
  font_path += "assets/fonts/font.ttf";
  
  if ( !m_font.loadFromFile(font_path) )
    {
      throw std::invalid_argument("RenderEngine cannot load assets/fonts/font.ttf."); 
    }

  m_text.setFont(m_font);
  
}

EventStatus RenderEngine::getWindow(Event& event)
{
  GetWindowEvent& gwe = static_cast<GetWindowEvent&>(event);

  gwe.window = static_cast<sf::RenderWindow const*>(m_window.get());

  return EventStatus::Continue;
}

EventStatus RenderEngine::add(Event& event)
{
  PictureEvent &picture = static_cast<PictureEvent&>(event);
  
  m_pictures.push_back( picture );
  return EventStatus::Continue;
}


EventStatus RenderEngine::addText(Event& event)
{
  TextEvent &te = static_cast<TextEvent&>(event);
  
  m_textEvents.push_back( te );
  return EventStatus::Continue;
}

EventStatus RenderEngine::clearText(Event& event)
{
  m_textEvents.clear();
  return EventStatus::Continue;
}


void RenderEngine::display()
{
  m_window->clear(sf::Color::White);
  
   // Sorting all the pictures to draw by z-index
  std::sort(m_pictures.begin(), m_pictures.end(), [](auto& p1, auto& p2){
      return p1.z_index < p2.z_index;
    });

  // Draw the pictures
  for(auto& pict : m_pictures)
    {
      m_rectangle.setFillColor(pict.color);
      m_rectangle.setOrigin(pict.w/2, pict.h/2);
      m_rectangle.setPosition(sf::Vector2f(pict.x, pict.y));
      m_rectangle.setTexture( &m_resources->getTexture(pict.id), true );
      m_rectangle.setSize(sf::Vector2f(pict.w, pict.h));
      m_window->draw(m_rectangle);

      //      std::cout<< "id : " << pict.id << " x : " << pict.x << " y : " << pict.y << "w : " << pict.w <<"h : " << pict.h <<  std::endl;
    }
  
  // Draw the texts
  for(auto& textEvent : m_textEvents)
    {
      m_text.setString(textEvent.text);
      m_text.setPosition(textEvent.x, textEvent.y);
      m_text.setCharacterSize(textEvent.size);
      m_text.setColor(textEvent.color);
      m_window->draw(m_text);
    }
  
  m_pictures.clear();

  m_window->display();
}

RenderEngine::~RenderEngine()
{
  
}
