#include <TimeService.hpp>

TimeService::TimeService()
{
   m_timer.restart(); 
}

void TimeService::waitAnd(float time, func_t fun, void* data)
{
  m_functions.push_back(std::make_tuple(time + m_timer.getElapsedTime().asMilliseconds(), fun, data));
}

void TimeService::update()
{
  for(auto& f_info : m_functions)
    {
      if( m_timer.getElapsedTime().asMilliseconds() >= std::get<0>(f_info) )
	{
	  std::get<1>(f_info)(std::get<2>(f_info));
	  m_functions.clear();
	  m_timer.restart();
	  
	}
    }
}


TimeService::~TimeService()
{
  
}
