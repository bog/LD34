#include <ConsoleLogService.hpp>

ConsoleLogService::ConsoleLogService()
  : LogService()
{
  
}

/*virtual*/ void ConsoleLogService::write(std::string msg)
{
  std::cout<< "-> " <<msg <<std::endl;
}

/*virtual*/ void ConsoleLogService::error(std::string msg)
{
  std::cout<< "E : " <<msg <<std::endl;
}

/*virtual*/ void ConsoleLogService::warning(std::string msg)
{
  std::cout<< "W : " <<msg <<std::endl;
}
  

ConsoleLogService::~ConsoleLogService()
{
  
}
