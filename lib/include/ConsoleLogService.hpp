/* Copyright (C) 2015  WalgrinTeam

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* LogService */
#ifndef CONSOLELOGSERVICE_HPP
#define CONSOLELOGSERVICE_HPP
#include <iostream>
#include <Service.hpp>
#include <LogService.hpp>

class ConsoleLogService : public LogService
{
public:
  ConsoleLogService();
  virtual ~ConsoleLogService();
  
  virtual void write(std::string msg) override;
  virtual void error(std::string msg) override;
  virtual void warning(std::string msg) override;
  
protected:

private:
  ConsoleLogService( ConsoleLogService const& consolelogservice ) = delete;
  ConsoleLogService& operator=( ConsoleLogService const& consolelogservice ) = delete;
  
  
};

#endif
