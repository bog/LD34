/* Scene */
#ifndef SCENE_HPP
#define SCENE_HPP
#include <iostream>
#include <Entity.hpp>

class Scene : public Entity
{
public:
  Scene();
  virtual ~Scene();

  virtual void update(float dt) override;
  virtual void display() override;

  inline virtual bool isActivated(){return m_activated;}
  inline virtual void activate(){m_activated = true;}
  inline virtual void desactivate(){m_activated = false;}
  
protected:
  bool m_activated;
private:
  Scene( Scene const& scene ) = delete;
  Scene& operator=( Scene const& scene ) = delete;
  
  
};

#endif
