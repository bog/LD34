/* Manager */
#ifndef MANAGER_HPP
#define MANAGER_HPP
#include <iostream>
#include <Entity.hpp>

class Manager : public Entity
{
public:
  Manager();
  virtual ~Manager();

  virtual void update(float dt) override = 0;
  virtual void display() override = 0;
protected:
  
private:
  Manager( Manager const& manager ) = delete;
  Manager& operator=( Manager const& manager ) = delete;
  
  
};

#endif
