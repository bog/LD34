/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* MusicEngine */
#ifndef MUSICENGINE_HPP
#define MUSICENGINE_HPP
#include <iostream>
#include <Engine.hpp>
#include <memory>
#include <ServiceLocator.hpp>
#include <SFML/Audio/Music.hpp>

class ResourceSystem;

class MusicEngine : public Engine
{
public:
  explicit MusicEngine(std::shared_ptr<ResourceSystem> resources);

  EventStatus play(Event& event);
  
  virtual ~MusicEngine();
  
protected:
  std::shared_ptr<ResourceSystem> m_resources;
  sf::Music m_music;
  
private:
  MusicEngine( MusicEngine const& musicengine ) = delete;
  MusicEngine& operator=( MusicEngine const& musicengine ) = delete;
  
  
};

#endif
