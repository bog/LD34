/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* UtilityService */
#ifndef UTILITYSERVICE_HPP
#define UTILITYSERVICE_HPP
#include <iostream>
#include <Service.hpp>
#include <vector>
#include <fstream>
#include <stdexcept>
#include <functional>

class UtilityService : public Service
{
public:
  explicit UtilityService();

    std::vector<std::string> split(std::string str, char separator, char endSeparator) const;

  template<typename O, typename M>
  void load(const std::string file, O* const object, M method);
  
  std::string nucleus(std::string path);
  std::string nucleusAndExtension(std::string path);

  virtual ~UtilityService();
  
protected:
  
private:
  UtilityService( UtilityService const& utilityservice ) = delete;
  UtilityService& operator=( UtilityService const& utilityservice ) = delete;
  
  
};

template<typename O, typename M>
void UtilityService::load(const std::string file, O* const object, M method)
{
  std::string line;
  std::fstream reader(file, std::ios::in);
  if( !reader )
    {
      throw std::invalid_argument("[UtilityService] File : " + file + " couldn't be read.");
    }
  
  while ( getline(reader, line) )
    {
      std::bind(method, object, std::placeholders::_1)(line);
    }
  reader.close();
}


#endif
