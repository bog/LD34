/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* ControlEngine */
#ifndef CONTROLENGINE_HPP
#define CONTROLENGINE_HPP
#include <iostream>
#include <ServiceLocator.hpp>
#include <SFML/System.hpp>

#include <GameEvents.hpp>

class ControlEngine
{
public:
  explicit ControlEngine();
  virtual ~ControlEngine();
  
protected:
  int m_key_count;
  int m_key_delay;
  Key m_key_previous;
private:
  ControlEngine( ControlEngine const& controlengine ) = delete;
  ControlEngine& operator=( ControlEngine const& controlengine ) = delete;
  
  
};

#endif
