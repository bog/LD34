/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* SoundEngine */
#ifndef SOUNDENGINE_HPP
#define SOUNDENGINE_HPP
#include <iostream>
#include <Engine.hpp>
#include <memory>
#include <ServiceLocator.hpp>
#include <SFML/Audio.hpp>

#define MAX_STORED_SOUND_NUMBER 50

class ResourceSystem;

class SoundEngine : public Engine
{
public:
  explicit SoundEngine(std::shared_ptr<ResourceSystem> resources);

  EventStatus play(Event& e);
  
  virtual ~SoundEngine();
  
protected:
  void clearStopped(void);
  
  std::shared_ptr<ResourceSystem> m_resources;
  std::vector<sf::Sound> m_sounds;
private:
  SoundEngine( SoundEngine const& soundengine ) = delete;
  SoundEngine& operator=( SoundEngine const& soundengine ) = delete;
  
  
};

#endif
