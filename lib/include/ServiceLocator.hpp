/* ServiceLocator */
#ifndef SERVICELOCATOR_HPP
#define SERVICELOCATOR_HPP
#include <iostream>
#include <memory>
#include <EventService.hpp>
#include <UtilityService.hpp>
#include <LogService.hpp>
#include <ConsoleLogService.hpp>
#include <TimeService.hpp>

class ServiceLocator
{
public:

  virtual ~ServiceLocator();

  static ServiceLocator* get()
  {
    if( !ServiceLocator::m_instance )
      {
	ServiceLocator::m_instance.reset(new ServiceLocator());
      }

    return ServiceLocator::m_instance.get();
  }

  /* event */
  void provide(EventService *es)
  {
    if( !es ){ throw std::invalid_argument("E : Bad event service provided"); }
    m_event.reset(es);
  }
  
  EventService* event()
  {
    if( !m_event.get() )
      {
	throw std::invalid_argument("E : Cannot find event service !");
      }
    
    return m_event.get();
  }

  /* utility */
  void provide(UtilityService *us)
  {
    if( !us ){ throw std::invalid_argument("E : Bad utility service provided"); }
    m_utility.reset(us);
  }

  /* time */
  void provide(TimeService *ts)
  {
    if( !ts ){ throw std::invalid_argument("E : Bad time service provided"); }
    m_time.reset(ts);
  }

  /* log */
  void provide(LogService *ls)
  {
    if( !ls ){ throw std::invalid_argument("E : Bad log service provided"); }
    m_log.reset(ls);
  }
  
  LogService* log()
  {
    if( !m_log.get() )
      {
	throw std::invalid_argument("E : Cannot find log service !");
      }
    
    return m_log.get();
  }

  
  UtilityService* utility()
  {
    if( !m_utility.get() )
      {
	throw std::invalid_argument("E : Cannot find utility service !");
      }
    
    return m_utility.get();
  }

  TimeService* time()
  {
    if( !m_time.get() )
      {
	throw std::invalid_argument("E : Cannot find time service !");
      }
    
    return m_time.get();
  }

  
protected:
  ServiceLocator();
  static std::unique_ptr<ServiceLocator> m_instance;

  std::unique_ptr<EventService> m_event;
  std::unique_ptr<UtilityService> m_utility;
  std::unique_ptr<LogService> m_log;
  std::unique_ptr<TimeService> m_time;
  
private:
  ServiceLocator( ServiceLocator const& servicelocator ) = delete;
  ServiceLocator& operator=( ServiceLocator const& servicelocator ) = delete;
  
  
};

extern std::unique_ptr<ServiceLocator> sl;

#endif
