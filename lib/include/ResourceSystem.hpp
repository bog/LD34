/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* ResourceSystem */
#ifndef RESOURCESYSTEM_HPP
#define RESOURCESYSTEM_HPP
#include <iostream>
#include <unordered_map>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

#define RSC_PATH "../"
/// Resources which are level independants
class ResourceSystem
{
public:
  explicit ResourceSystem();

  sf::Texture&  getTexture(std::string id);
  sf::FloatRect& getTextureCoords(std::string id);
  sf::SoundBuffer& getSoundBuffer(std::string id);
  std::string getMusicPath(std::string id);
  
  virtual ~ResourceSystem();
  
protected:
  void loadTexture(std::string line);
  void loadSound(std::string line);
  void loadMusic(std::string line);

  std::unordered_map<std::string, sf::Texture> m_textures;
  std::unordered_map<std::string, sf::FloatRect> m_texCoords;

  std::unordered_map<std::string, sf::SoundBuffer> m_sounds;

  std::unordered_map<std::string, std::string> m_musicPathes;
  
private:
  ResourceSystem( ResourceSystem const& resourcesystem ) = delete;
  ResourceSystem& operator=( ResourceSystem const& resourcesystem ) = delete;
  
  
};

#endif
