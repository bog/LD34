/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* TimeService */
#ifndef TIMESERVICE_HPP
#define TIMESERVICE_HPP
#include <iostream>
#include <functional>
#include <SFML/System.hpp>
#include <vector>
#include <tuple>

typedef std::function<void(void*)> func_t ;

class TimeService
{
public:
  explicit TimeService();
  virtual ~TimeService();

  void waitAnd(float time, func_t fun, void* data);
  void update();
  
protected:
  sf::Clock m_timer;
  std::vector<std::tuple<float, func_t, void*> > m_functions;
private:
  TimeService( TimeService const& timeservice ) = delete;
  TimeService& operator=( TimeService const& timeservice ) = delete;
  
  
};

#endif
