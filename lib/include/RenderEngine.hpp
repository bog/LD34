/* RenderEngine */
#ifndef RENDERENGINE_HPP
#define RENDERENGINE_HPP
#include <iostream>
#include <Engine.hpp>
#include <SFML/Graphics.hpp>
#include <memory>
#include <vector>
#include <ServiceLocator.hpp>

class ResourceSystem;

class RenderEngine : public Engine
{
public:
  RenderEngine(std::shared_ptr<sf::RenderWindow> window, std::shared_ptr<ResourceSystem> resources);
  virtual ~RenderEngine();

  EventStatus add(Event& event);
  EventStatus addText(Event& event);
  EventStatus clearText(Event& event);
  EventStatus getWindow(Event& event);
  
  void display();
  
protected:
  std::shared_ptr<sf::RenderWindow> m_window;
  std::shared_ptr<ResourceSystem> m_resources;

  std::vector<PictureEvent> m_pictures;
  std::vector<TextEvent> m_textEvents;
  sf::RectangleShape m_rectangle;

  sf::Text m_text;
  sf::Font m_font;
private:
  RenderEngine( RenderEngine const& renderengine ) = delete;
  RenderEngine& operator=( RenderEngine const& renderengine ) = delete;
  
};

#endif
