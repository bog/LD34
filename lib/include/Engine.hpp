/* Engine */
#ifndef ENGINE_HPP
#define ENGINE_HPP
#include <iostream>

class Engine
{
public:
  Engine();
  virtual ~Engine();
  
protected:
  
private:
  Engine( Engine const& engine ) = delete;
  Engine& operator=( Engine const& engine ) = delete;
  
  
};

#endif
