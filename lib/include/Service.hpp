/* Service */
#ifndef SERVICE_HPP
#define SERVICE_HPP
#include <iostream>

class Service
{
public:
  Service();
  virtual ~Service();
  
protected:
  
private:
  Service( Service const& service ) = delete;
  Service& operator=( Service const& service ) = delete;
  
  
};

#endif
