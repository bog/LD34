/* Copyright (C) 2015  WalgrinTeam

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* Event */
#ifndef EVENT_HPP
#define EVENT_HPP
#include <iostream>
#include <SFML/Graphics.hpp>

enum class EventStatus
{
  Continue,
    Kill
    };

class Event
{
public:
  explicit Event() = default;
  virtual ~Event(){};
  
protected:

private:
  // Event( Event const& event ) = delete;            // We might need to copy
  // Event& operator=( Event const& event ) = delete; // We might need to copy
  
  
};

/* Core */

//
struct MousePressedEvent : public Event
{
  MousePressedEvent()
    : x(0.0f)
    , y(0.0f)
    , screen_x(0.0f)
    , screen_y(0.0f)
    , left(false)
    , right(false)
  {
    
  }
  
  float x;
  float y;

  float screen_x;
  float screen_y;
  
  bool left;
  bool right;
};

//
struct TextEvent : public Event
{
  TextEvent(float _x, float _y, std::string _text, size_t _size = 32, sf::Color _color = sf::Color::White):
    x(_x)
    , y(_y)
    , text(_text)
    , size(_size)
    , color(_color)
  {}
    
  float x;
  float y;
  std::string text;
  size_t size;
  sf::Color color;
};
  
//
struct KeyPressedEvent : public Event
{
  KeyPressedEvent(sf::Keyboard::Key k) : key(k) {}
  sf::Keyboard::Key key;
};
  
/* Sound Event */
struct SoundEvent : public Event
{
  SoundEvent(std::string _id) : id(_id) {}
  std::string id;
};

/* Music Event */
struct MusicEvent : public SoundEvent
{
  MusicEvent(std::string id ) : SoundEvent(id) {}
};

/* Render Event */
struct PictureEvent : public Event
{
  PictureEvent(float _x, float _y, float _w, float _h, int _z, std::string _id, sf::Color _c)
    : x(_x)
    , y(_y)
    , w(_w)
    , h(_h)
    , z_index(_z)
    , id(_id)
    , color( _c)
  {
  }


  PictureEvent(float _x, float _y, float _w, float _h, int _z, std::string _id)
    : x(_x)
    , y(_y)
    , w(_w)
    , h(_h)
    , z_index(_z)
    , id(_id)
    , color(sf::Color::White)
  {
  }

  PictureEvent(float _x, float _y, float _w, float _h)
    : PictureEvent(_x, _y, _w, _h, 0, "null", sf::Color::White)
  {
  }
  
  float x;
  float y;
  float w;
  float h;

  int z_index;
  std::string id;
  sf::Color color;
};

// getWindow

struct GetWindowEvent : public Event
{
  GetWindowEvent(sf::RenderWindow const* _window)
    : window(_window)
  {
  }

  GetWindowEvent()
    : window(nullptr)
  {
  }
  sf::RenderWindow const* window;
};


#endif
