/* Entity */
#ifndef ENTITY_HPP
#define ENTITY_HPP
#include <iostream>

class Entity
{
public:
  Entity() = default;
  virtual ~Entity(){}

  virtual void update(float dt) = 0;
  virtual void display() = 0;
protected:
  
private:
  Entity( Entity const& entity ) = delete;
  Entity& operator=( Entity const& entity ) = delete;
  
  
};

#endif
