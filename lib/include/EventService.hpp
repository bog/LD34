/* Copyright (C) 2015  WalgrinTeam

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* EventService */
#ifndef EVENTSERVICE_HPP
#define EVENTSERVICE_HPP
#include <iostream>
#include <Service.hpp>
#include <functional>
#include <memory>
#include <unordered_map>
#include <vector>
#include <Event.hpp>

// Definition of function allowed to be stored
typedef std::function<EventStatus(Event&)> EventHandler;

class EventService : public Service
{
public:
  EventService();
  virtual ~EventService();

  void subscribe(const std::string id, const EventHandler handler);

  // Subscribe with class method 
  template<typename O, typename M>
  void subscribe(const std::string id, O* object, M O::* method) 
  {
    subscribe( id, std::bind(method, object, std::placeholders::_1) );
  }

  void trigger(const std::string id);
  void trigger(const std::string id, Event& event);

  
protected:
  std::unordered_map<std::string, std::vector<EventHandler> > m_subscribers;

private:
  EventService( EventService const& eventservice ) = delete;
  EventService& operator=( EventService const& eventservice ) = delete;
  
  
};

#endif
