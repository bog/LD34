#include <Core.hpp>
#include <Scene.hpp>
#include <LevelScene.hpp>
#include <MenuScene.hpp>
#include <EndScene.hpp>

Core::Core()
  : m_running( true )
  , m_window( std::make_shared<sf::RenderWindow>(sf::VideoMode(WINDOW_WIDTH
							       , WINDOW_HEIGHT)
						 , "LD34") )
  , m_key_repeat_delay(125.0)
{
  m_window->setFramerateLimit(60.0);

  // SUBSCRIBE
  sl->event()->subscribe("core:changeScene", this, &Core::changeScene);
  sl->event()->subscribe("core:quit", [this](Event &event){
      m_running = false;
      return EventStatus::Kill;
    });
  
  // ENGINES
  m_resource_system = std::make_shared<ResourceSystem>();
  
  m_render_engine = std::make_unique<RenderEngine>(m_window , m_resource_system);
  m_control_engine = std::make_unique<ControlEngine>();
  m_music_engine = std::make_unique<MusicEngine>(m_resource_system);
  
  MusicEvent me("music1");
  sl->event()->trigger("music:play", me);
  
  // SCENE
  m_scenes["menu"] = std::make_unique<MenuScene>();
  m_scenes["level"] = std::make_unique<LevelScene>();
  m_scenes["end_congrats"] = std::make_unique<EndScene>(true);
  m_scenes["end_game_over"] = std::make_unique<EndScene>(false);

  m_current_scene = m_scenes["menu"].get();

  m_current_scene->activate();
}

EventStatus Core::changeScene(Event &event)
{
  ChangeSceneEvent& cse = static_cast<ChangeSceneEvent&>(event);
  auto itr = m_scenes.find(cse.scene);
  
  if( itr != m_scenes.end() )
    {
      cse.from->desactivate();
      m_current_scene = itr->second.get();
      m_current_scene->activate();
    }
  else
    {
      throw std::invalid_argument("Scene " + cse.scene + " unknown");
    }
  
  return EventStatus::Continue;
}

void Core::update()
{
  if(m_current_scene != nullptr)
    {
      m_current_scene->update(0.0);
    }

  // Keys
  // quit
  if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)
     || (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::C))
     || (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::Q)))
    {
      m_running = false;
    }

  // left, right or both
  if(m_key_repeat_clock.getElapsedTime().asMilliseconds() >= m_key_repeat_delay)
    {
      KeyEvent ke;
      bool trigger = false;
      // both
      if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left)
	 && sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
	  ke.key = Key::Both;
	  trigger = true;
	}
      // only left
      else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
	  ke.key = Key::Left;
	  trigger = true;
	}
      // only right
      else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
	  ke.key = Key::Right;
	  trigger = true;
	}

      if(trigger){ sl->event()->trigger("key:pressed", ke); }
      
      m_key_repeat_clock.restart();
    }
}

void Core::display()
{
  if(m_current_scene != nullptr)
    {
      m_current_scene->display();
    }
}

void Core::mainLoop()
{
  sf::Event event;
  while( m_running )
    {
      while( m_window->pollEvent(event) )
	{
	  switch( event.type )
	    {
	    case sf::Event::Closed:
	      m_running = false;
	      break;
	    default: ;
	    }
	}

      update();
      display();
      m_render_engine->display();
      sl->time()->update();
    }

  m_window->close();
}

Core::~Core()
{

}
