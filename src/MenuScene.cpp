#include <MenuScene.hpp>
#include <LevelScene.hpp>

MenuScene::MenuScene()
{
  sl->event()->subscribe("key:pressed",this, &MenuScene::onClick);
}

EventStatus MenuScene::onClick(Event& event)
{
  if( !m_activated ){ return EventStatus::Continue; }
  
  KeyEvent &ke = static_cast<KeyEvent&>(event);
      
  if(ke.key == Key::Left)
    {
      sl->event()->trigger("core:quit");
    }
  else if(ke.key == Key::Right)
    {
      ChangeSceneEvent cse(this, "level");
      sl->event()->trigger("core:changeScene", cse);
    }
      
  return EventStatus::Continue;
}

/*virtual*/ void MenuScene::update(float dt)
{
  
}

/*virtual*/ void MenuScene::display()
{
  PictureEvent pe(WINDOW_WIDTH/2
		  , WINDOW_HEIGHT/2
		  , WINDOW_WIDTH
		  , WINDOW_HEIGHT
		  , 0
		  , "menu");
  
  sl->event()->trigger("render:add", pe);
}

MenuScene::~MenuScene()
{
  
}
