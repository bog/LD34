#include <Person.hpp>

/*explicit*/ Person::Person(std::string id, float x, float y)
  : m_id(id)
  , m_position(sf::Vector2f(x, y))
  , m_size(sf::Vector2f(WINDOW_WIDTH/6.0, WINDOW_HEIGHT/2.5))
{
  
}

/*virtual*/ void Person::update(float dt)
{
  
}

/*virtual*/ void Person::display()
{
  PictureEvent pe(m_position.x, m_position.y, m_size.x, m_size.y, 50, m_id);
  sl->event()->trigger("render:add", pe);
}

Person::~Person()
{
  
}
