#include <Table.hpp>
#include <chrono>
#include <random>
#include <MenuScene.hpp>
#include <EndScene.hpp>

Table::Table(Scene* owner)
  : m_owner(owner)
  , m_size(sf::Vector2f(3*WINDOW_WIDTH/4, 2*WINDOW_HEIGHT/3))
  , m_position(sf::Vector2f(WINDOW_WIDTH/2, 3*WINDOW_HEIGHT/4))
  , m_rotating(false)
  , m_count_stop_rotating(0)
{
  // EVENT
  sl->event()->subscribe("key", this, &Table::rotate);
  sl->event()->subscribe("plate:stopped", this, &Table::plateStopped);
  
  // PLATES
  initPlatesNameDescription();
  initPlatesQuotes();
  shufflePlates();
}

void Table::shufflePlates()
{
  std::mt19937 random(std::chrono::steady_clock::now().time_since_epoch().count());
  std::shuffle(m_plates.begin(), m_plates.end(), random);

  initPlatesPosition();
}

void Table::initPlatesPosition()
{
  // bottom
  m_plates[0]->setPosition(m_position.x
			   , m_position.y - m_size.y/4);
  
  // left
  m_plates[1]->setPosition(m_position.x - 3*m_size.x/10
			   , m_position.y - m_size.y/8 - m_size.y/4);

  // top
  m_plates[2]->setPosition(m_position.x,
			   m_position.y - m_size.y/2);

  // right
  m_plates[3]->setPosition(m_position.x + 3*m_size.x/10
			   , m_position.y - m_size.y/8 - m_size.y/4);
}

void Table::initPlatesQuotes()
{
  //turkey
  m_plates[0]->addQuote("Humm.. It seems to be heavy..");
  m_plates[0]->addQuote("I just don't know..");
  m_plates[0]->addQuote("Hmmm...");
  m_plates[0]->addQuote("This plate is more hot than the others, it isn't ?");
  m_plates[0]->addQuote("Hmmm... This plate just seems... normal !");
  m_plates[0]->addQuote("Humm.. It could be the Turkey, I think...");
  
  //ant
  m_plates[1]->addQuote("It seems a little bit less heavy than the last plate..");
  m_plates[1]->addQuote("I just don't know..");
  m_plates[1]->addQuote("Hmmm...");
  m_plates[1]->addQuote("Hmmm... It is not hot anymore.");
  m_plates[1]->addQuote("Hmmm... This plate just seems... normal !");
  m_plates[1]->addQuote("It makes some noise while shaking it.");
  
  //dog
  m_plates[2]->addQuote("Humm.. It seems to be a bit heavy..");
  m_plates[2]->addQuote("I'd bet it contains a kind of liquid thing !");
  m_plates[2]->addQuote("I just don't know..");
  m_plates[2]->addQuote("Hmmm...");
  m_plates[2]->addQuote("This plate is more hot than the others, it isn't ?");
  m_plates[2]->addQuote("Humm.. It could be the Turkey, I think...");
  
  //pig
  m_plates[3]->addQuote("I can't decide what it could contain !");
  m_plates[3]->addQuote("Humm.. It could be the Turkey, I think...");
  m_plates[3]->addQuote("I just don't know..");
  m_plates[3]->addQuote("Hmmm...");
  m_plates[3]->addQuote("Hmmm... It is not so hot...");
  m_plates[3]->addQuote("It makes some noise while shaking it.");
}

void Table::initPlatesNameDescription()
{
  m_plates.push_back(std::make_shared<Plate>("Christmas Turkey"
					     , "turkey"));
  
  m_plates.push_back(std::make_shared<Plate>("Ant eggs with mayonnaise"
					     , "ants"));
  
  m_plates.push_back(std::make_shared<Plate>("Dog soup"
					     , "chien"));
  
  m_plates.push_back(std::make_shared<Plate>("Pig gums"
					     , "porc"));
}

EventStatus Table::plateStopped(Event &event)
{
  m_count_stop_rotating++;
  
  if( m_count_stop_rotating >= N_PLATE )
    {
      m_count_stop_rotating = 0;
      m_rotating = false;
    }

  return EventStatus::Continue;
}

EventStatus Table::rotate(Event &event)
{
  if(!m_owner->isActivated()){return EventStatus::Continue;}
  KeyEvent &ke = static_cast<KeyEvent&>(event);
  
  switch(ke.key)
    {
    case Key::Both:
      {
	if(m_plates[0]->getName() == "Christmas Turkey")
	  {
	    ChangeSceneEvent cse(m_owner, "end_congrats");
	    sl->event()->trigger("core:changeScene", cse);
	  }
	// LOSE
	else
	  {
	    ChangeSceneEvent cse(m_owner, "end_game_over");
	    sl->event()->trigger("core:changeScene", cse);
	  }
      }
      break;

    case Key::Left:
      rotateClockwise();
      break;

    case Key::Right:
      rotateAntiClockwise();
      break;
    }

  sl->event()->trigger("render:clearText");
  TextEvent te(8, 8, m_plates[0]->randomQuote(), WINDOW_WIDTH/28, sf::Color::Blue);
  sl->event()->trigger("render:addText", te);
  
  return EventStatus::Continue;
}

/*virtual*/ void Table::update(float dt)
{
  for(std::shared_ptr<Plate> plate: m_plates)
    {
      plate->update(dt);
    }
}

/*virtual*/ void Table::display()
{
  PictureEvent pe(m_position.x, m_position.y, m_size.x, m_size.y, 100, "table");
  sl->event()->trigger("render:add", pe);

  for(std::shared_ptr<Plate> plate: m_plates)
    {
      plate->display();
    }
}

void Table::rotateClockwise()
{
  if(m_rotating){return;}
  
  m_rotating = true;
  sf::Vector2f first_pos;
    
  first_pos.x = m_plates[0]->getX();
  first_pos.y = m_plates[0]->getY();
  
  for(size_t i=0; i<N_PLATE-1; i++)
    {
      m_plates[i]->goTo(m_plates[i+1]->getX(), m_plates[i+1]->getY());
    }

  m_plates[N_PLATE-1]->goTo(first_pos.x, first_pos.y);
  std::rotate(m_plates.begin(), m_plates.begin()+1, m_plates.end());
}

void Table::rotateAntiClockwise()
{
  if(m_rotating){return;}
  
  m_rotating = true;
  sf::Vector2f previous_pos;
  
  previous_pos.x = m_plates[N_PLATE - 1]->getX();
  previous_pos.y = m_plates[N_PLATE - 1]->getY();
  
  for(size_t i=0; i<N_PLATE; i++)
    {
      m_plates[i]->goTo(previous_pos.x, previous_pos.y);
      
      previous_pos.x = m_plates[i]->getX();
      previous_pos.y = m_plates[i]->getY();
    }
  
  std::rotate(m_plates.begin(), m_plates.end()-1, m_plates.end());
}

Table::~Table()
{
  
}
