#include <Plate.hpp>
#include <stdexcept>
#include <chrono>
#include <random>

/*explicit*/ Plate::Plate(std::string name, std::string description)
  : Plate(name, description, 0.0, 0.0)
{

}

/*explicit*/ Plate::Plate(std::string name, std::string description, float x, float y)
  : m_size(sf::Vector2f(WINDOW_WIDTH/10, WINDOW_WIDTH/10))
  , m_position(sf::Vector2f(x, y))
  , m_destination(m_position)
  , m_velocity(sf::Vector2f(2, 2))
  , m_error_tolerance(2)
  , m_moving(false)
  , m_name(name)
  , m_description(description)
{
  
}

std::string Plate::randomQuote()
{
  if(m_quotes.empty()){ return "???"; }
  
  std::mt19937 random(std::chrono::steady_clock::now().time_since_epoch().count());
  std::uniform_int_distribution<int> uid(0, m_quotes.size()-1);

  return m_quotes[uid(random)];
}

void Plate::addQuote(std::string str)
{
  m_quotes.push_back(str);
}

void Plate::setPosition(float x, float y)
{
  m_position = sf::Vector2f(x, y);
}

/*virtual*/ void Plate::update(float dt)
{
  if( m_moving )
    {
      bool stopped = true;
      if( std::abs(m_position.x - m_destination.x) > m_error_tolerance )
	{
	  if( m_position.x < m_destination.x ){m_position.x += m_velocity.x;}
	  if( m_position.x > m_destination.x ){m_position.x -= m_velocity.x;}
	  stopped = false;
	}
  
      if( std::abs(m_position.y - m_destination.y) > m_error_tolerance )
	{
	  if( m_position.y < m_destination.y ){m_position.y += m_velocity.y;}
	  if( m_position.y > m_destination.y ){m_position.y -= m_velocity.y;}
	  stopped = false;
	}

      if( stopped )
	{
	  StopRotatingEvent sre(this);
	  sl->event()->trigger("plate:stopped", sre);
	  m_moving = false;
	  m_position.x = m_destination.x;
	  m_position.y = m_destination.y;
	}
    }
}

float Plate::getX()
{
  return m_position.x;
}

float Plate::getY()
{
  return m_position.y;
}

void Plate::goTo(float x, float y)
{
  if( x < 0 || x >= WINDOW_WIDTH || y < 0 || y >= WINDOW_HEIGHT )
    {
      throw std::invalid_argument("Can't move a plate in that place ("
				  + std::to_string(x)
				  +", "
				  + std::to_string(y)
				  +") !");
    }
  
  m_destination.x = x;
  m_destination.y = y;

  m_moving = true;
}

/*virtual*/ void Plate::display()
{
  PictureEvent pe(m_position.x, m_position.y, m_size.x, m_size.y, 150,  "plate");
  sl->event()->trigger("render:add", pe);
}

std::string Plate::getName()
{
  return m_name;
}

Plate::~Plate()
{
  
}
