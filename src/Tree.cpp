#include <Tree.hpp>

Tree::Tree(float x, float y)
  : m_id("tree")
  , m_position(sf::Vector2f(x, y))
  , m_size(sf::Vector2f(WINDOW_WIDTH/1.8, WINDOW_HEIGHT/1.1))
  , m_frame(0)
  , m_tree_delay(500)
{
  
}

/*virtual*/ void Tree::update(float dt)
{
  if(m_tree_clock.getElapsedTime().asMilliseconds() > m_tree_delay)
    {
      m_frame++;
      m_frame %= 2;

      m_tree_clock.restart();
    }
}

/*virtual*/ void Tree::display()
{
  PictureEvent pe(m_position.x
		  , m_position.y
		  , m_size.x
		  , m_size.y
		  , 10
		  , m_id + std::to_string(m_frame+1));
  
  sl->event()->trigger("render:add", pe);
}

Tree::~Tree()
{
  
}
