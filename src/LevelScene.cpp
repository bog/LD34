#include <LevelScene.hpp>
#include <ServiceLocator.hpp>
#include <Table.hpp>
#include <Plate.hpp>
#include <Person.hpp>
#include <Tree.hpp>

LevelScene::LevelScene()
{
  m_table = new Table(this);
  m_entities.push_back( std::shared_ptr<Table>(m_table) );
  m_entities.push_back( std::make_shared<Person>("person1"
						 , WINDOW_WIDTH/2
						 , WINDOW_HEIGHT/3) );
  
  m_entities.push_back( std::make_shared<Person>("person2",
						 WINDOW_WIDTH/6,
						 WINDOW_HEIGHT/2) );

  m_entities.push_back( std::make_shared<Person>("person3",
						 5*WINDOW_WIDTH/6,
						 WINDOW_HEIGHT/2) );

  m_entities.push_back( std::make_shared<Tree>(5*WINDOW_WIDTH/6,
						 3*WINDOW_HEIGHT/6) );
}

/*virtual*/ void LevelScene::activate()
{
  Scene::activate();
  m_table->shufflePlates();
}

/*virtual*/ void LevelScene::update(float dt)
{
  for(std::shared_ptr<Entity> entity : m_entities)
    {
      entity->update(dt);
    }
}

/*virtual*/ void LevelScene::display()
{
  for(std::shared_ptr<Entity> entity : m_entities)
    {
      entity->display();
    }

  PictureEvent pe(WINDOW_WIDTH/2, WINDOW_HEIGHT/2
		  , WINDOW_WIDTH, WINDOW_HEIGHT, 0, "bg");
  sl->event()->trigger("render:add", pe);
}

LevelScene::~LevelScene()
{
  
}
