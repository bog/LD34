#include <iostream>
#include <ServiceLocator.hpp>
#include <Core.hpp>

int main()
{
  sl->provide(new EventService());
  sl->provide(new UtilityService());
  sl->provide(new ConsoleLogService());
  sl->provide(new TimeService());
  
  {
    Core core;
    core.mainLoop();
  }
  
  sl.release();
  return 0;
}
