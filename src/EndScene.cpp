#include <EndScene.hpp>
#include <Core.hpp>
#include <MenuScene.hpp>

EndScene::EndScene(bool won)
{
  won ? m_id = "congrats" : m_id = "game_over";
  
  sl->event()->subscribe("key:pressed", [this](Event &event){
      if( !m_activated ){ return EventStatus::Continue; }
      ChangeSceneEvent cse(this, "menu");
      sl->event()->trigger("core:changeScene", cse);
      return EventStatus::Continue;
    });
}

/*virtual*/ void EndScene::update(float dt)
{
  sl->event()->trigger("render:clearText");
}

/*virtual*/ void EndScene::display()
{ 
  PictureEvent pe(WINDOW_WIDTH/2
		    , WINDOW_HEIGHT/2
		    , WINDOW_WIDTH
		    , WINDOW_HEIGHT
		    , 0
		    , m_id);

  sl->event()->trigger("render:add", pe);
}



EndScene::~EndScene()
{
  
}
