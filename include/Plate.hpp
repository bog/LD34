/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* Plate */
#ifndef PLATE_HPP
#define PLATE_HPP
#include <iostream>
#include <Entity.hpp>
#include <Core.hpp>

class Plate : public Entity
{
public:
  explicit Plate(std::string name, std::string description);
  explicit Plate(std::string name, std::string description, float x, float y);
  virtual ~Plate();

  virtual void update(float dt) override;
  virtual void display() override;

  void goTo(float x, float y);

  float getX();
  float getY();
  std::string getName();
  void setPosition(float x, float y);
  void addQuote(std::string str);
  std::string randomQuote();
  
protected:
  sf::Vector2f m_position;
  sf::Vector2f m_size;
  
  sf::Vector2f m_destination;
  sf::Vector2f m_velocity;

  std::string m_name;
  std::string m_description;
  
  float m_error_tolerance;

  bool m_moving;

  std::vector<std::string> m_quotes;
private:
  Plate( Plate const& plate ) = delete;
  Plate& operator=( Plate const& plate ) = delete;
  
};

#endif
