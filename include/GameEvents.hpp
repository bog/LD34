/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* GameEvents */
#ifndef GAMEEVENTS_HPP
#define GAMEEVENTS_HPP
#include <Event.hpp>

enum class Key
{
  None,
    Left,
    Right,
    Both
};

struct KeyEvent : public Event
{
  KeyEvent()
    : KeyEvent(Key::None)
  {
  }
  
  KeyEvent(Key k)
    : key(k)
  {
    
  }

  Key key;
};

class Plate;

struct StopRotatingEvent : public Event
{
  StopRotatingEvent(Plate* p)
    : plate(p)
  {
  }

  Plate* plate;
};

class Scene;
struct ChangeSceneEvent : public Event
{
  ChangeSceneEvent(Scene* f, std::string s)
    : from(f)
    , scene(s)
  {
  }
  
  Scene* from;
  std::string scene;

};
#endif
