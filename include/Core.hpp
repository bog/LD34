/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* Core */
#ifndef CORE_HPP
#define CORE_HPP
#include <iostream>
#include <RenderEngine.hpp>
#include <ControlEngine.hpp>
#include <MusicEngine.hpp>
#include <ResourceSystem.hpp>
#include <ServiceLocator.hpp>
#include <SFML/Graphics.hpp>
#include <GameEvents.hpp>
#include <vector>
#include <unordered_map>

#define WINDOW_WIDTH 640
#define WINDOW_HEIGHT 480

class Scene;

class Core
{
public:
  explicit Core();
  virtual ~Core();

  void mainLoop();
  
protected:
  void update();
  void display();
  EventStatus changeScene(Event &event);
private:
  Core( Core const& core ) = delete;
  Core& operator=( Core const& core ) = delete;

  bool m_running;
  std::shared_ptr<sf::RenderWindow> m_window;
  std::unique_ptr<RenderEngine> m_render_engine;
  std::unique_ptr<ControlEngine> m_control_engine;
  std::shared_ptr<ResourceSystem> m_resource_system;
  std::unique_ptr<MusicEngine> m_music_engine;
  
  Scene* m_current_scene;
  std::unordered_map<std::string, std::unique_ptr<Scene> > m_scenes;
  
  
  sf::Clock m_key_repeat_clock;
  float m_key_repeat_delay;
  
};

#endif
