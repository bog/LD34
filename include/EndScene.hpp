/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* EndScene */
#ifndef ENDSCENE_HPP
#define ENDSCENE_HPP
#include <iostream>
#include <Scene.hpp>

class EndScene : public Scene
{
public:
  explicit EndScene(bool won);
  virtual ~EndScene();


  virtual void update(float dt) override;
  virtual void display() override;
  
  
protected:

  std::string m_id;
private:
  EndScene( EndScene const& endscene ) = delete;
  EndScene& operator=( EndScene const& endscene ) = delete;
  
  
};

#endif
