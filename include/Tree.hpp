/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* Tree */
#ifndef TREE_HPP
#define TREE_HPP
#include <iostream>
#include <Core.hpp>
#include <Entity.hpp>

class Tree : public Entity
{
public:
  explicit Tree(float x, float y);
  virtual ~Tree();

  virtual void update(float dt) override;
  virtual void display() override;
protected:
  std::string m_id;
  sf::Vector2f m_position;
  sf::Vector2f m_size;
  
  int m_frame;
  sf::Clock m_tree_clock;
  float m_tree_delay;
private:
  Tree( Tree const& tree ) = delete;
  Tree& operator=( Tree const& tree ) = delete;
  
  
};

#endif
