/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* Person */
#ifndef PERSON_HPP
#define PERSON_HPP
#include <iostream>
#include <Core.hpp>
#include <Entity.hpp>

#include <SFML/Graphics.hpp>

class Person : public Entity
{
public:
  explicit Person(std::string id, float x, float y);
  virtual ~Person();

  virtual void update(float dt) override;
  virtual void display() override;

protected:
  std::string m_id;
  sf::Vector2f m_position;
  sf::Vector2f m_size;
private:
  Person( Person const& person ) = delete;
  Person& operator=( Person const& person ) = delete;
  
  
};

#endif
