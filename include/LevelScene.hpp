/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* LevelScene */
#ifndef LEVELSCENE_HPP
#define LEVELSCENE_HPP
#include <iostream>
#include <Core.hpp>
#include <Scene.hpp>

class Table;

class LevelScene : public Scene
{
public:
  explicit LevelScene();
  virtual ~LevelScene();

  virtual void update(float dt) override;
  virtual void display() override;

  virtual void activate() override;
protected:
  std::vector<std::shared_ptr<Entity>> m_entities;
  Table* m_table;
private:
  LevelScene( LevelScene const& levelscene ) = delete;
  LevelScene& operator=( LevelScene const& levelscene ) = delete;
  
  
};

#endif
