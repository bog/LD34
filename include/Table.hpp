/* Copyright (C) <year>  <name of author>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

/* Table */
#ifndef TABLE_HPP
#define TABLE_HPP
#include <iostream>
#include <Core.hpp>
#include <Manager.hpp>
#include <Plate.hpp>

#define N_PLATE 4

class Table : public Manager
{
public:
  explicit Table(Scene* owner);
  virtual ~Table();
  
  virtual void update(float dt) override;
  virtual void display() override;
  
  void rotateClockwise();
  void rotateAntiClockwise();
  EventStatus rotate(Event &event);
  EventStatus plateStopped(Event &event);

  void shufflePlates();
protected:
  void initPlatesQuotes();
  void initPlatesPosition();
  void initPlatesNameDescription();

  Scene* m_owner;
  sf::Vector2f m_position;
  sf::Vector2f m_size;
  std::vector<std::shared_ptr<Plate> > m_plates;

  bool m_rotating;
  int m_count_stop_rotating;
private:
  Table( Table const& table ) = delete;
  Table& operator=( Table const& table ) = delete;
  
};

#endif
